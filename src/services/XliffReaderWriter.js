import Vue from "vue";

export default {
  // readXML(xmlFile){
  //     var parser = new DOMParser();
  //     var reader = new FileReader();
  //     var promise = new Promise(function(resolve, reject){
  //         reader.onload = function(){
  //             var result = parser.parseFromString(reader.result, "text/xml");
  //             console.log(result);
  //             return this.result;
  //         };
  //         reader.readAsText(xmlFile);
  //     });

  readXMLFile(xmlFile) {
    return new Promise((resolve, reject) => {
      var parser = new DOMParser();
      var reader = new FileReader();
      reader.onload = () => {
        var result = parser.parseFromString(reader.result, "text/xml");
        resolve(result);
      };
      reader.readAsText(xmlFile);
    });
  },

  writeToXMLFile(xmlDoc, xmlName) {
    var serializer = new XMLSerializer();
    var xmlString = serializer.serializeToString(xmlDoc.documentElement);
    var xmlFile = new File([xmlString], {
      type: "text/xml"
    });
    if (window.navigator.msSaveOrOpenBlob)
      // IE10+
      window.navigator.msSaveOrOpenBlob(xmlFile, xmlName);
    else {
      // Others
      var a = document.createElement("a"),
        url = URL.createObjectURL(xmlFile);
      a.href = url;
      a.download = xmlName;
      document.body.appendChild(a);
      a.click();
      setTimeout(function() {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }
  },

  xml2String(xmlDoc){
    var serializer = new XMLSerializer();
    var xmlString = serializer.serializeToString(xmlDoc.documentElement);
    return xmlString;
  },

  string2Xml(xmlString){
    var parser = new DOMParser();
    var result = parser.parseFromString(xmlString, "text/xml");
    return result;
  }
};
